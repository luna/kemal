# THIS IS A FORK OF KEMAL

This is the outline of the changes done in this fork, compared to Kemal upstream:

- Default HEAD request removed has been removed. If you wish to provide it,
  you must give your own handler.
  - Can not be upstreamed as it would be a breaking change that I did not
    ask upstream about. This is a very specific issue to the web services I
    write.
- Fix to prevent overwriting the `content-length` header value when set by the
  request handler.
  - Could be upstreamed, as it is a single line fix.
- Websockets can be closed via an exception, with support for reasons.
  - Can not be upstreamed. Same reasoning as the first change.

## Installation?

In your shard.yml:

```yaml
dependencies:
  spec-kemal:
    git: https://gitdab.com/luna/spec-kemal.git
  kemal:
    git: https://gitdab.com/luna/kemal.git
```
